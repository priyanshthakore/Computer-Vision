import cv2

cap = cv2.VideoCapture(0)

#supported resolutions
# 320x240,  640,480 , 1280x720
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

frame_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
frame_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
fps = cap.get(cv2.CAP_PROP_FPS)

print("Width is  %s \nHeight is %s \nFPS is %s \n" % (frame_width, frame_height, fps))

while(True):
    ret, frame = cap.read()
    if ret == True:
        cv2.imshow('frame', frame)
    else:
        print("Failed to get frame")

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()