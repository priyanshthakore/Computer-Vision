import cv2

# prints Opencv Version
opencv_version =  format(cv2.__version__)
print('OpenCV version: %s' % (opencv_version))