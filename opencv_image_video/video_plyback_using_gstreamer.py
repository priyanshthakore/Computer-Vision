'''

You need to have Gstreamer installed 

'''

import cv2
import os
import imutils

os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;tcp"

# Uncommenting this will start usb camera
cap = cv2.VideoCapture('v4l2src device=/dev/video0 ! video/x-raw,framerate=20/1 ! videoscale ! videoconvert ! appsink', cv2.CAP_GSTREAMER)

# Uncommenting this will start RTSP camera
# cap = cv2.VideoCapture('rtspsrc location=rtsp://192.168.1.51:8080/h264_ulaw.sdp latency=20 ! rtph264depay ! h264parse ! omxh264dec ! nvvidconv ! video/x-raw, width=(int)640, height=(int)480, format=(string)BGRx ! videoconvert ! appsink', cv2.CAP_GSTREAMER)


while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret==True:
        # Display the resulting frame
        frame = imutils.resize(frame, width=800)
        cv2.imshow('frame',frame)

    else:
        # Uncommenting this will start usb camera
        cap = cv2.VideoCapture('v4l2src device=/dev/video0 ! video/x-raw,framerate=20/1 ! videoscale ! videoconvert ! appsink', cv2.CAP_GSTREAMER)

        # Uncommenting this will start RTSP camera
        # cap = cv2.VideoCapture('rtspsrc location=rtsp://admin:incerno123@192.168.1.36:554/cam/realmonitor?channel=1&subtype=1&unicast=true&proto=Onvif latency=20 ! rtph264depay ! h264parse ! omxh264dec ! nvvidconv ! video/x-raw, width=(int)640, height=(int)480, format=(string)BGRx ! videoconvert ! appsink',           cv2.CAP_GSTREAMER)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()