import cv2

# Load Images
frame = cv2.imread('image.jpg')

# convert it to grayscale
gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

# write the greyscale image
cv2.imwrite('image_greyscale.jpg', gray)

# Display both frames Images
cv2.imshow('Display_Image', frame)
cv2.imshow('Greyscale', gray)

# wait till q is pressed
while (cv2.waitKey(1) & 0xFF != ord('q')):
    pass

cv2.destroyAllWindows()