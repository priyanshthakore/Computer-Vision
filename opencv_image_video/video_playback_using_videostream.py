
# in order to get ret working need to change make modification in imtuils VideoStream package

from imutils.video import VideoStream
import cv2

vs = VideoStream(src=0).start()
while True:
    ret, frame = vs.read()
    if ret == True:
        cv2.imshow("Frame", frame)
    else:
        print("Failed to get Frame")
    key = cv2.waitKey(1) & 0xFF
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

cv2.destroyAllWindows()
vs.stop()