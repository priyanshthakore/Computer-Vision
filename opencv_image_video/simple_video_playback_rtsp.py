import cv2
import os

os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;tcp"

cap = cv2.VideoCapture('rtsp://port:ip/')
while True:
    ret, frame = cap.read()
    if ret == True:
        cv2.imshow('frame', frame)
    else:
        print("Failed to get frame")

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()